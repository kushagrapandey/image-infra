package main

import (
	"fmt"
	"image-infra/tools"
	"net/http"
	"github.com/h2non/bimg"
)

var projectId string = "staging-bikayi"
var bucketName string = "bikayi-stg-cdn"

func compress(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		fmt.Println("Invalid path: ", r.URL.Path)
		http.NotFound(w, r)
		return
	}

	query := r.URL.Query();
	fmt.Println(query);
	fileName, ok := query["fileName"];

	if !ok {
		fmt.Println("filename parameter not found");
		http.NotFound(w, r);
		return;
	}

	imgprm := tools.GetValidParams(w, r);
	file, err := tools.GetFile(projectId, bucketName, fileName[0]);
	

	if err != nil {
		fmt.Println(err,file)
		http.NotFound(w, r)
		return
	}
	
	buf,err := bimg.Read(fileName[0]);
	if err != nil {
		fmt.Println(err);
		return;
	}
	newImage,err := bimg.NewImage(buf).Convert(bimg.ImageType(imgprm.ImageFormatId));
	
	if err!=nil {
		fmt.Println(err);
		w.Write(newImage);
		return;
	}

	w.Write(newImage);

}

func main() {
	http.HandleFunc("/", compress)
	http.ListenAndServe(":8080", nil)
}

