
const getValidParams = (req)=>{
    let height = null, width = null, format = null;

    if(req.query.width)
    width = parseInt(req.query.width);
    if(req.query.height)
    height = parseInt(req.query.height);
    if(req.query.format)
    format = req.query.format;

    const validFormat = ['jpeg','png','format'];        

    if(!validFormat.includes(format))
    format = 'jpeg';

    height = Math.min(height,10000);
    width = Math.min(width,10000);

    return {"height":height,"width":width,"format":format};

}

module.exports = {getValidParams};
