package tools

import (
	"fmt"
	"math"
	"net/http"
	"strconv"
)


func GetValidParams(w http.ResponseWriter, r *http.Request) ImageParams {
	query := r.URL.Query();
	var height,width int;

	height, err1 :=  strconv.Atoi(query.Get("height"));
	width, err2 := strconv.Atoi(query.Get("width"));
	var format string = query.Get("format");

	
	imageFormatMap := [4]string{"jpeg", "jpeg", "webp", "png"} ;
	imageFormatId := 0;

	if err1 != nil {
		height = 0;
	}
	if err2 != nil {
		width = 0;
	}

	for i :=0; i<4; i++ {
		if imageFormatMap[i]==format{
			imageFormatId = i;
		}
	}

	height = int(math.Min(10000,float64(height)));
	width = int(math.Min(10000,float64(width)));

	fmt.Println("width :", width, " height :", height);

	image := ImageParams{
		Height: height,
		Width: width,
		ImageFormatId : imageFormatId,
	}

	fmt.Println("mm ",height,width,format);
	return image;
	
}
