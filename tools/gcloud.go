package tools;

import (
	"context"
	"fmt"
	"io"
	"os"
	"cloud.google.com/go/storage"
)



func GetFile(projectId string, bucketName string, fileName string) (*storage.Reader,error) {
	ctx := context.Background();
	client, err := storage.NewClient(ctx);

	if err != nil {
		fmt.Println(err);
		return nil,err;
	}
	defer client.Close();
	fmt.Println(bucketName);
	bucket := client.Bucket(bucketName);
	bucket.UserProject(projectId);
	file,err := bucket.Object(fileName).NewReader(ctx);

	if err!=nil {
		return nil,err;
	}

	fo, err := os.Create(fileName)
    if err != nil {
        panic(err)
    }
    defer func() {
        if err := fo.Close(); err != nil {
            panic(err)
        }
    }()


	buf := make([]byte, 1024)
    for {
        // read a chunk
        n, err := file.Read(buf)
        if err == io.EOF {
			break;
		}
		if err != nil {
			fmt.Println(err);
			break;
		}

        // write a chunk
        if _, err := fo.Write(buf[:n]); err != nil {
            panic(err)
        }
    }
	file.Close();

	return file,nil;
}