const express = require("express");
const {getValidParams} = require('./tools.js');
const sharp = require('sharp');
const app = express();

const {Storage} = require('@google-cloud/storage');
const { pipeline } = require("stream");
const { env } = require("process");
const projectId = 'staging-bikayi';
const PORT = process.env.PORT||3000;
const bucketName =process.env.bucketName||'bikayi-stg-cdn';


const storage = new Storage({
    projectId:projectId
});

app.get('/',(req,res)=>{
    const fileName = req.query.fileName;
    
    if(!fileName){
        console.log('no filename parameter found in query object\n');
        res.status(404).send('<h2>Oops! filename parameter not found in request object</h2>');
    }
    else
        useSharp(fileName,req,res);
})

app.use((req,res)=>{
    res.sendFile('./Error/404.html',{root:__dirname});
})


async function useSharp(fileName, req, res){

    const id = await storage.getProjectId();

    const bucket = storage.bucket(bucketName);      
    const file = bucket.file(fileName);

    const readableStream = file.createReadStream();
    
    const queryParams = getValidParams(req);

    // instantiating sharp object
    const data = sharp()
    .resize({
        width : queryParams.width||null,
        height : queryParams.height||null
    })
    .toFormat(queryParams.format)
    .toBuffer(function (err,data,info){
        if(err){
            console.log(err)
            res.status(500).send(err.message);
        }
    });

    // handling error if failure happen during pipeline transfer
    readableStream.on("error", function(err) {
        console.error(err);
        res.status(404).send(err.message);
    });

    //creating pipeline
    try {
        await pipeline(readableStream, data, res,(err)=>{
        console.log(err);
    });
    } catch (err) {

    } 

}


app.listen(PORT);